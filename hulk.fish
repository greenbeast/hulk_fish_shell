set fish_color_command 1c6d0a
set fish_color_param brpurple
set fish_color_error brown

function fish_prompt
         set sign ""

         if [ "$USER" = "root" ]
                set sign ":#"
         else
                set sign ':≡'
         end

         set cwd (string replace /home/$USER "~" $PWD)
         set machineName (cat /proc/sys/kernel/hostname)
         echo (set_color brmagenta)$cwd(set_color 98ec85)$sign" "
end


