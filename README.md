# Hulk fish shell

![Alt text](hulk_fish_shell.png)

Very basic fish shell inspired by the colors of the Incredible Hulk. To apply these changes to your shell just go to `~/.config/fish/functions/fish_prompt.fish` and replace the code with the code in the `fish_prompt.fish` script here. I am not even sure how to add this to OMF but maybe I'll look into that.